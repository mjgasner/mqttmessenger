/***
Copyright (c) 2008-2012 CommonsWare, LLC
Licensed under the Apache License, Version 2.0 (the "License"); you may not
use this file except in compliance with the License. You may obtain a copy
of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
by applicable law or agreed to in writing, software distributed under the
License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
OF ANY KIND, either express or implied. See the License for the specific
language governing permissions and limitations under the License.

From _The Busy Coder's Guide to Advanced Android Development_
  http://commonsware.com/AdvAndroid
*/

package com.gasner.c2dm;

import android.R;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.gasner.mqttmessenger.MQTTMessengerActivity;
import com.google.android.c2dm.C2DMBaseReceiver;

public class C2DMReceiver extends C2DMBaseReceiver {
  public C2DMReceiver() {
    super("gasnersoftwaretest@gmail.com");
  }

  @Override
  public void onRegistered(Context context, String registrationId) {
    Log.w("C2DMReceiver-onRegistered", registrationId);
  }
  
  @Override
  public void onUnregistered(Context context) {
    Log.w("C2DMReceiver-onUnregistered", "got here!");
  }
  
  @Override
  public void onError(Context context, String errorId) {
    Log.w("C2DMReceiver-onError", errorId);
  }
  
  @Override
  protected void onMessage(Context context, Intent intent) {
	  
	String ns = context.NOTIFICATION_SERVICE;
	NotificationManager mNotificationManager = (NotificationManager) getSystemService(ns);
	
	int icon = R.drawable.ic_lock_idle_alarm;
	CharSequence tickerText = "Message = " + intent.getStringExtra("payload");
	long when = System.currentTimeMillis();

	Notification notification = new Notification(icon, tickerText, when);
	
	CharSequence contentTitle = "C2DMTest";
	CharSequence contentText = tickerText;
	Intent notificationIntent = new Intent(this, MQTTMessengerActivity.class);
	PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

	notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);
	
	final int HELLO_ID = 2;
	mNotificationManager.notify(HELLO_ID, notification);
	
    Log.w("C2DMReceiver", intent.getStringExtra("payload"));
  }
}