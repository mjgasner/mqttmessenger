package com.gasner.mqttmessenger;

import java.util.ArrayList;

import sun.security.action.GetLongAction;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.gasner.sqlite.*;

public class ContactAdapter extends ArrayAdapter<Contact> {
	private ArrayList<Contact> contactList;
	private Context context;
	
	public ContactAdapter(Context context, int textViewResourceId, ArrayList<Contact> contactList){
		super(context, textViewResourceId, contactList);
		this.contactList = contactList;
		this.context = context;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		if (v == null){
			LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = (View)vi.inflate(R.layout.contact, null);
		}
		Contact c = contactList.get(position);
		if(c != null) {
			TextView nametv = (TextView) v.findViewById(R.id.nametv);
			TextView counttv = (TextView) v.findViewById(R.id.unreadcounttv);
			
			if(nametv != null) {
				nametv.setText(c.getName());
				nametv.setTag(c.getId());
			}
			if(counttv != null) {
				counttv.setText(String.valueOf(c.getUnreadCount()));
			}
		}
		
		return v;
	}
	
}
