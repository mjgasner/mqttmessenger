package com.gasner.mqttmessenger;

import java.util.ArrayList;

import com.gasner.sqlite.Contact;
import com.gasner.sqlite.Message;

import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ConversationActivity extends ListActivity {

	public static final String UIMESSAGELIST = "com.gasner.mqttmessenger.conversationactivity.uimessagelist";
	private Intent intent;
	
	private ArrayList<Message> messageList;
	private MessageAdapter messageAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.conversation);
		
		final EditText messagetb = (EditText) findViewById(R.id.messagetb);
		final Button sendBtn = (Button)findViewById(R.id.sendbtn);
		sendBtn.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				String message = messagetb.getText().toString();
				
				MessengerService.actionSendMessage(getApplicationContext(), message);
				
				messagetb.setText("");
			}
		});
		
		RegisterBroadcasts();
		
		long contactID = Long.valueOf(getIntent().getAction());
		
		ConversationHelper.DisplayConversations(contactID, this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		RegisterBroadcasts();
	}

	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(broadcastReceiver);
	}
	
	private void RegisterBroadcasts(){
		registerReceiver(broadcastReceiver, new IntentFilter(UIMESSAGELIST));
	}
	
	private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			
			if(action.equals(UIMESSAGELIST)) {
				Logger.log("uimessagelist - Broadcast Received");
				UpdateMessageUI(intent);
			}
		}
	};
    
    private void UpdateMessageUI(Intent intent) {        
    	//Get Contact List back out of ParcelableArrayList
    	messageList = new ArrayList<Message>();
    	messageList = intent.getParcelableArrayListExtra("messages");    	
    	
    	this.messageAdapter = new MessageAdapter(this, R.layout.conversation, messageList);
    	setListAdapter(this.messageAdapter);
    }	
}
