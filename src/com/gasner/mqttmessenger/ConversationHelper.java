package com.gasner.mqttmessenger;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;

import com.gasner.sqlite.ContactsDataAccess;
import com.gasner.sqlite.MessagesDataAccess;

public class ConversationHelper {
	public static void DisplayConversations(long contactID, Context context){
		Logger.log("Getting Convo with contactid - " + String.valueOf(contactID));
		
		//Get all messages for this contact
		ArrayList<com.gasner.sqlite.Message> messageList = MessagesDataAccess.getMessagesByContactId(context, contactID);
		
		//Tell UI Service to Tell UI To Update
		Intent intent = new Intent(ConversationActivity.UIMESSAGELIST);
		intent.putParcelableArrayListExtra("messages", messageList);
		
		context.sendBroadcast(intent);
	}

}
