package com.gasner.mqttmessenger;

import java.io.IOException;

import android.util.Log;

public class Logger {
	// Connection log. Good for debugging.
	//private static ConnectionLog 			mLog;
	
	// log helper function
	public static void log(String message) {
		log(message, null);
	}
	public static void log(String message, Throwable e) {
//		try {
//		mLog = new ConnectionLog();
//		
//		Log.i(Constants.PREF_NAME, "Opened log at " + mLog.getPath());
//		} catch (IOException e1) {
//			Log.e(Constants.PREF_NAME, "Failed to open log", e1);
//		}
		
		if (e != null) {
			Log.e(Constants.PREF_NAME, message, e);
			
		} else {
			Log.i(Constants.PREF_NAME, message);			
		}
		
//		if (mLog != null)
//		{
//			try {
//				mLog.println(message);
//			} catch (IOException ex) {}
//		}
//		try {
//		if (mLog != null)
//			mLog.close();
//	} catch (IOException e1) {}	
	}
}
