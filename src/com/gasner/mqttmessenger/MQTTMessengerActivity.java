package com.gasner.mqttmessenger;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.gasner.sqlite.Contact;
import com.gasner.sqlite.ContactsDataSource;
import com.google.android.c2dm.C2DMessaging;

public class MQTTMessengerActivity extends ListActivity {
	
	public static final String UICONTACTLIST = "com.gasner.mqttmessenger.mqttmessengeractivity.uicontactlist";
	public static final String UICONNECTIONSTATUS = "com.gasner.mqttmessenger.mqttmessengeractivity.uiconnectionstatus";
	private Intent intent;
	
	private ArrayList<Contact> contactList;
	private ContactAdapter contactAdapter;

	private TextView connStatus;
	private Button openBtn;
	private Button closeBtn;
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);        
        
        RegisterBroadcasts();
        
        //Open MQTT connection
        MessengerService.actionStart(getApplicationContext());
        
        //Update UI
        MessengerHelper.UpdateContactList(this);
        
        //Bind Button Events
    	((Button) findViewById(R.id.openbtn)).setOnClickListener(OpenBtnClick);
    	((Button) findViewById(R.id.closebtn)).setOnClickListener(CloseBtnClick);
        
        if(false){
	        //For Testing/////////////////////////////////////////////////////
	        Intent intent = new Intent();									//
	        contactList = new ArrayList<Contact>();							//
	        int i = 0;														//	
	        for(i=0;i<10;i++){												//
	        	Contact contact = new Contact();							//
	        	contact.setId(i);											//
	        	contact.setGmail("bob" + String.valueOf(i));				//
	        	contact.setName("bob" + String.valueOf(i));					//	
	        	contact.setUnreadCount(i);									//
	        																//
	        	contactList.add(contact);									//				
	        }																//
	        																//	
	        intent.putParcelableArrayListExtra("contacts", contactList);	//
	        UpdateContactUI(intent);										//
	        //////////////////////////////////////////////////////////////////
        }
    }
    
	@Override
	protected void onResume() {
		super.onResume();
		RegisterBroadcasts();
	}

	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(broadcastReceiver);
	}
    
	private void RegisterBroadcasts(){
		registerReceiver(broadcastReceiver, new IntentFilter(UICONTACTLIST));
		registerReceiver(broadcastReceiver, new IntentFilter(UICONNECTIONSTATUS));
	}
	
	private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			
			if(action.equals(UICONTACTLIST)) {
				Logger.log("uicontactlist - Broadcast Received");
				UpdateContactUI(intent);
			}
			if(action.equals(UICONNECTIONSTATUS)){
				connStatus = (TextView) findViewById(R.id.connstatustv);
				
				int isConnected = intent.getIntExtra("isConnected", 0);
				if(isConnected == 1){
					//Connected
					connStatus.setText("Connected");
					connStatus.setTextColor(getResources().getColor(R.color.Green));
				}
				else if(isConnected == 0) {
					//Disconnected
					connStatus.setText("Disconnected");
					connStatus.setTextColor(getResources().getColor(R.color.Gray));
				}
				else if(isConnected == 2) {
					//Connecting
					connStatus.setText("Connecting...");
					connStatus.setTextColor(getResources().getColor(R.color.Yellow));
				}
				else if(isConnected == 3){
					//Failed
					connStatus.setText("Failed");
					connStatus.setTextColor(getResources().getColor(R.color.Red));
				}
			}
		}
	};
	
	private View.OnClickListener OpenBtnClick = new View.OnClickListener() {
		public void onClick(View v) {
			MessengerService.actionStart(getApplicationContext());
		}
	};
		
	private View.OnClickListener CloseBtnClick = new View.OnClickListener() {
		public void onClick(View v) {
			MessengerService.actionStop(getApplicationContext());
		}
	};
	
    
    private void UpdateContactUI(Intent intent) {        
    	//Get Contact List back out of ParcelableArrayList
    	contactList = new ArrayList<Contact>();
    	contactList = intent.getParcelableArrayListExtra("contacts");    	
    	
    	this.contactAdapter = new ContactAdapter(this, R.layout.contact, contactList);
    	setListAdapter(this.contactAdapter);
    }

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		
		TextView nametv = (TextView) v.findViewById(R.id.nametv);
		
		intent = new Intent(nametv.getTag().toString());
		intent.setClass(this, ConversationActivity.class);
		
		startActivity(intent);
	}
    
    
}