package com.gasner.mqttmessenger;

import java.util.ArrayList;

import com.gasner.sqlite.Message;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MessageAdapter extends ArrayAdapter<Message> {
	private ArrayList<Message> messageList;
	private Context context;
	
	public MessageAdapter(Context context, int textViewResourceId, ArrayList<Message> messageList){
		super(context, textViewResourceId, messageList);
		this.messageList = messageList;
		this.context = context;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		//View v = convertView;
		//if(v == null){
			LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View v = vi.inflate(R.layout.message, null);
		//}
		Message m = messageList.get(position);
		if(m != null) {
			TextView messagetv = (TextView) v.findViewById(R.id.messagetv);
			TextView timetv = (TextView) v.findViewById(R.id.timetv);
			
			if(messagetv != null){
				messagetv.setText(m.getMessage());
				if(m.getFromMe() == 1) {
					messagetv.setTextColor(context.getResources().getColor(R.color.Red));
					
					LinearLayout layout = (LinearLayout) v.findViewById(R.id.messagelayout);
					layout.setHorizontalGravity(Gravity.RIGHT);
					
					messagetv.setGravity(Gravity.RIGHT);
					timetv.setGravity(Gravity.RIGHT);
				}
			}
			if(timetv != null){
				timetv.setText(m.getTime());
			}
			
		}
		
		return v;
	}
}
