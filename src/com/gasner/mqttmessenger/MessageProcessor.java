package com.gasner.mqttmessenger;

import java.util.ArrayList;
import java.util.List;

import org.fusesource.hawtbuf.Buffer;
import org.fusesource.hawtbuf.UTF8Buffer;

import android.content.Context;
import android.content.Intent;

import com.gasner.sqlite.ContactsDataAccess;
import com.gasner.sqlite.ContactsDataSource;
import com.gasner.sqlite.MessagesDataAccess;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class MessageProcessor {
	public static void process(UTF8Buffer topic, Buffer payload, Context context) {
		
		Logger.log("Message Recieved");
		Logger.log("topic - " + topic.toString());
		Logger.log("payload - " + payload.ascii().toString());
		Logger.log("Message Purpose - " + topic.toString().split("/")[2]);
		Gson gson = new Gson();
		
		//Get Message "data"
		String data = payload.ascii().toString();
		//Get Message Purpose
		String messagePurpose = topic.toString().split("/")[2];
		
		if(messagePurpose.equals("messagedump")) {	
			//Save Messages to DB
			List<com.gasner.sqlite.QTMessage> messageList = gson.fromJson(data, new TypeToken<List<com.gasner.sqlite.QTMessage>>(){}.getType());
			
			MessagesDataAccess.AddNewMessages(context, messageList);
			
			Logger.log("Message Dump - DONE");
			
			MessengerHelper.UpdateContactList(context);
		}
		if(messagePurpose.equals("message")) {
			Logger.log("messagepurpose was message");
			com.gasner.sqlite.QTMessage message = gson.fromJson(data, com.gasner.sqlite.QTMessage.class);
			MessagesDataAccess.createMessage(context, MessagesDataAccess.getContactIdByName(context, message.getName()), message.getMessage(), message.getTime(), 0);
			Logger.log(String.valueOf(MessagesDataAccess.getAllMessages(context).size()));				
		}
	}
}
