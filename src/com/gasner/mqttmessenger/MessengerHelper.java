package com.gasner.mqttmessenger;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;

import com.gasner.sqlite.ContactsDataAccess;

public class MessengerHelper {
	public static void UpdateContactList(Context context){
		//GetAllContacts
		ArrayList<com.gasner.sqlite.Contact> contactList = ContactsDataAccess.getAllContacts(context);
		
		//Tell UI Service to Tell UI To Update
		Intent intent = new Intent(MQTTMessengerActivity.UICONTACTLIST);
		intent.putParcelableArrayListExtra("contacts", contactList);
		
		context.sendBroadcast(intent);
	}
}
