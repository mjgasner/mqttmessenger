package com.gasner.mqttmessenger;

import java.io.IOException;
import java.util.List;

import org.fusesource.hawtbuf.UTF8Buffer;
import org.fusesource.hawtbuf.Buffer;
import org.fusesource.mqtt.client.BlockingConnection;
import org.fusesource.mqtt.client.Callback;
import org.fusesource.mqtt.client.CallbackConnection;
import org.fusesource.mqtt.client.Listener;
import org.fusesource.mqtt.client.MQTT;
import org.fusesource.mqtt.client.QoS;
import org.fusesource.mqtt.client.Topic;
import org.fusesource.*;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.os.Message;
import android.provider.Contacts.OrganizationColumns;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import com.gasner.sqlite.*;

public class MessengerService extends Service {
	
	public static String CLIENT_ID = "mqttmessenger";
	
	public static String APP_TAG = "mqttmessenger";
	public static String HOSTIP = "206.123.56.3";
	public static int PORT = 1883;
	
	public static short KEEP_ALIVE = 60 * 15;
	
	// These are the actions for the service (name are descriptive enough)
	private static final String		ACTION_START = CLIENT_ID + ".START";
	private static final String		ACTION_STOP = CLIENT_ID + ".STOP";
	private static final String		ACTION_KEEPALIVE = CLIENT_ID + ".KEEP_ALIVE";
	private static final String		ACTION_RECONNECT = CLIENT_ID + ".RECONNECT";
	
	private static final String		ACTION_GETMESSAGES = CLIENT_ID + ".GETMESSAGES";
	private static final String  	ACTION_SENDMESSAGE = CLIENT_ID + ".SENDMESSAGE";
	
	private ConnectivityManager mConnMan;
	
	// Whether or not the service has been started.	
	private boolean 				mStarted;

	// This the application level keep-alive interval, that is used by the AlarmManager
	// to keep the connection active, even when the device goes to sleep.
	private static final long		KEEP_ALIVE_INTERVAL = 1000 * 60 * 28;

	// Retry intervals, when the connection is lost.
	private static final long		INITIAL_RETRY_INTERVAL = 1000 * 10;
	private static final long		MAXIMUM_RETRY_INTERVAL = 1000 * 60 * 30;

	private static boolean isConnected = false;
	
	// Preferences instance 
	private SharedPreferences 		mPrefs;
	// We store in the preferences, whether or not the service has been started
	public static final String		PREF_STARTED = "isStarted";
	// We also store the deviceID (target)
	public static final String		PREF_DEVICE_ID = "mqttdeviceid";
	// We store the last retry interval
	public static final String		PREF_RETRY = "retryInterval";
	
	// This is the instance of an MQTT connection.
	private CallbackConnection		mConnection;
	private long					mStartTime;
	
	// Static method to start the service
		public static void actionStart(Context ctx) {
			Intent i = new Intent(ctx, MessengerService.class);
			i.setAction(ACTION_START);
			ctx.startService(i);
		}

		// Static method to stop the service
		public static void actionStop(Context ctx) {
			Intent i = new Intent(ctx, MessengerService.class);
			i.setAction(ACTION_STOP);
			ctx.startService(i);
		}
		
		// Static method to send a keep alive message
		public static void actionPing(Context ctx) {
			Intent i = new Intent(ctx, MessengerService.class);
			i.setAction(ACTION_KEEPALIVE);
			ctx.startService(i);
		}
		
		// Static method to send a "Get Messages" publish to the server
		public static void actionGetMessages(Context ctx) {
			Intent i = new Intent(ctx, MessengerService.class);
			i.setAction(ACTION_GETMESSAGES);
			ctx.startService(i);
		}
		// Static method to send a "message" publish to the server
		public static void actionSendMessage(Context ctx, String Message) {
			Intent i = new Intent(ctx, MessengerService.class);
			i.setAction(ACTION_SENDMESSAGE);
			i.putExtra("message", Message);
			ctx.startService(i);
		}

		@Override
		public void onCreate() {
			super.onCreate();
			
			Logger.log("Creating service");
			mStartTime = System.currentTimeMillis();

			// Get instances of preferences, connectivity manager and notification manager
			mPrefs = getSharedPreferences(Constants.TAG, MODE_PRIVATE);
			mConnMan = (ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
		
			/* If our process was reaped by the system for any reason we need
			 * to restore our state with merely a call to onCreate.  We record
			 * the last "started" value and restore it here if necessary. */
			//handleCrashedService();
		}
		
		// This method does any necessary clean-up need in case the server has been destroyed by the system
		// and then restarted
		private void handleCrashedService() {
			if (wasStarted() == true) {
				Logger.log("Handling crashed service...");
				 // stop the keep alives
				stopKeepAlives(); 
					
				// Do a clean start
				start();
			}
		}
		
		@Override
		public void onDestroy() {
			Logger.log("Service destroyed (started=" + mStarted + ")");

			// Stop the services, if it has been started
			if (mStarted == true) {
				stop();
			}	
		}
		
		@Override
		public void onStart(Intent intent, int startId) {
			super.onStart(intent, startId);
			Logger.log("Service started with intent=" + intent);
			// Do an appropriate action based on the intent.
			if (intent.getAction().equals(ACTION_STOP) == true) {
				//stop();
				stopSelf();
			} else if (intent.getAction().equals(ACTION_START) == true) {
				start();
			} else if (intent.getAction().equals(ACTION_KEEPALIVE) == true) {
				keepAlive();
			} else if (intent.getAction().equals(ACTION_RECONNECT) == true) {
				if (isNetworkAvailable()) {
					reconnectIfNecessary();
				}
			} else if (intent.getAction().equals(ACTION_GETMESSAGES) == true) {
				publish(APP_TAG + "/server/getmessages", Constants.getDeviceID(getApplicationContext()));
			} else if (intent.getAction().equals(ACTION_SENDMESSAGE) == true) {
				publish(APP_TAG + "/server/tickerrequest", intent.getStringExtra("message"));
			}
		}
		
		@Override
		public IBinder onBind(Intent intent) {
			return null;
		}
		
		// Reads whether or not the service has been started from the preferences
		private boolean wasStarted() {
			return mPrefs.getBoolean(PREF_STARTED, false);
		}

		// Sets whether or not the services has been started in the preferences.
		private void setStarted(boolean started) {
			mPrefs.edit().putBoolean(PREF_STARTED, started).commit();		
			mStarted = started;
		}

		private synchronized void start() {
			Logger.log("Starting service...");
			
			// Do nothing, if the service is already running.
			//if (mStarted == true) {
				//Log.w(Constants.PREF_NAME, "Attempt to start connection that is already active");
				//return;
			//}
			
			// Establish an MQTT connection
			connect();
			
			// Register a connectivity listener
			//registerReceiver(mConnectivityChanged, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));		
		}

		private synchronized void stop() {
			// Do nothing, if the service is not running.
			if (mStarted == false) {
				Log.w(Constants.PREF_NAME, "Attempt to stop connection not active.");
				return;
			}

			// Save stopped state in the preferences
			setStarted(false);

			// Remove the connectivity receiver
			//unregisterReceiver(mConnectivityChanged);
			// Any existing reconnect timers should be removed, since we explicitly stopping the service.
			cancelReconnect();

			// Destroy the MQTT connection if there is one
			if (mConnection != null) {
				try {
					mConnection.disconnect(new Callback<Void>() {
						public void onSuccess(Void v) {
							//called once the connection is disconnected.
						}
						public void onFailure(Throwable value){
							//Disconnects never fail.
						}
					});
				}
				catch (Exception e){}
				mConnection = null;
			}
		}
		
		private synchronized void connect() {		
			Logger.log("Connecting...");
			// fetch the device ID from the preferences.
			//String deviceID = mPrefs.getString(PREF_DEVICE_ID, null);
			String deviceID = "bob";
			// Create a new connection only if the device id is not NULL
			if (deviceID == null) {
				Logger.log("Device ID not found.");
			} else {
				try {
					//Device doesn't support IPv6
					System.setProperty("java.net.preferIPv6Addresses", "false");
					
					MQTT mqtt = new MQTT();
					mqtt.setHost("tcp://" + HOSTIP + ":1883");
					
					mConnection = mqtt.callbackConnection();
					
					mConnection.listener(new Listener() {

					    public void onDisconnected() {
					    	Logger.log("Disconnected");
					    	isConnected = false;
					    
							//Tell UI Service to Tell UI To Update with connection Status
							Intent intent = new Intent(MQTTMessengerActivity.UICONNECTIONSTATUS);
							intent.putExtra("isConnected", 0);
							
							getApplicationContext().sendBroadcast(intent);
					    }
					    public void onConnected() {
					    	Logger.log("Connected");
					    	isConnected = true;

							//Tell UI Service to Tell UI To Update with connection Status
							Intent intent = new Intent(MQTTMessengerActivity.UICONNECTIONSTATUS);
							intent.putExtra("isConnected", 1);
							
							getApplicationContext().sendBroadcast(intent);
					    }

					    public void onPublish(UTF8Buffer topic, Buffer payload, Runnable ack) {
					        // You can now process a received message from a topic.
					        // Once process execute the ack runnable.
					    	
					    	MessageProcessor.process(topic, payload, getApplicationContext());
					    	
					        ack.run();
					    }
					    public void onFailure(Throwable value) {
					    	Logger.log("listener failed? - " + value.toString());
					    	//mConnection.close(null); // a connection failure occured.
					    }
					});
					
					//Tell UI Service to Tell UI To Update with connection Status
					Intent intent = new Intent(MQTTMessengerActivity.UICONNECTIONSTATUS);
					intent.putExtra("isConnected", 2);
					
					getApplicationContext().sendBroadcast(intent);
					
					mConnection.connect(new Callback<Void>() {
					    public void onFailure(Throwable value) {
					    	Logger.log("Connection Failed" + value.toString());
					    	
							//Tell UI Service to Tell UI To Update with connection Status
							Intent intent = new Intent(MQTTMessengerActivity.UICONNECTIONSTATUS);
							intent.putExtra("isConnected", 3);
							
							getApplicationContext().sendBroadcast(intent);
					    	
					        //result.failure(value); // If we could not connect to the server.
					    }

					    // Once we connect..
					    public void onSuccess(Void v) {
					    	subscribe(APP_TAG + "/" + Constants.getDeviceID(getApplicationContext()) + "/messagedump");
					    	subscribe(APP_TAG + "/" + Constants.getDeviceID(getApplicationContext()) + "/message");
					    	
					    	//publish(APP_TAG + "/server/userconnected", Constants.getDeviceID(getApplicationContext()));
					    }
					});
					
				} catch (Exception e) {
					// Schedule a reconnect, if we failed to connect
					Logger.log("MqttException: " + (e.getMessage() != null ? e.getMessage() : "NULL"));
		        	if (isNetworkAvailable()) {
		        		scheduleReconnect(mStartTime);
		        	}
				}
				setStarted(true);
			}
		}

		public synchronized void subscribe(final String topicName) {
			Topic topic = new Topic(topicName, QoS.AT_LEAST_ONCE);
			
			mConnection.subscribe(new Topic[] { topic }, new Callback<byte[]>() {
				public void onSuccess(byte[] value) {
					Logger.log("Subscribe to " + topicName + " Successful");
				}
				public void onFailure(Throwable value) {
					Logger.log("Subscribe to " + topicName + " Failed");
				}
			});
		}
		public synchronized void publish(final String topic, String message) {
			mConnection.publish(topic, message.getBytes(), QoS.EXACTLY_ONCE, false, new Callback<Void>() {
				public void onSuccess(Void v) {
					Logger.log("Publish to " + topic + " Successful");
				}
				public void onFailure(Throwable value) {
					Logger.log("Publish to " + topic + " Failed");
				}
			});
		}
		
		private synchronized void keepAlive() {
//			try {
//				// Send a keep alive, if there is a connection.
//				if (mStarted == true && mConnection != null) {
//					mConnection.sendKeepAlive();
//				}
//			} catch (MqttException e) {
//				Logger.log("MqttException: " + (e.getMessage() != null? e.getMessage(): "NULL"), e);
//				
//				mConnection.disconnect();
//				mConnection = null;
//				cancelReconnect();
//			}
		}

		// Schedule application level keep-alives using the AlarmManager
		private void startKeepAlives() {
//			Intent i = new Intent();
//			i.setClass(this, PushService.class);
//			i.setAction(ACTION_KEEPALIVE);
//			PendingIntent pi = PendingIntent.getService(this, 0, i, 0);
//			AlarmManager alarmMgr = (AlarmManager)getSystemService(ALARM_SERVICE);
//			alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP,
//			  System.currentTimeMillis() + KEEP_ALIVE_INTERVAL,
//			  KEEP_ALIVE_INTERVAL, pi);
		}

		// Remove all scheduled keep alives
		private void stopKeepAlives() {
//			Intent i = new Intent();
//			i.setClass(this, PushService.class);
//			i.setAction(ACTION_KEEPALIVE);
//			PendingIntent pi = PendingIntent.getService(this, 0, i, 0);
//			AlarmManager alarmMgr = (AlarmManager)getSystemService(ALARM_SERVICE);
//			alarmMgr.cancel(pi);
		}

		// We schedule a reconnect based on the starttime of the service
		public void scheduleReconnect(long startTime) {
//			// the last keep-alive interval
//			long interval = mPrefs.getLong(PREF_RETRY, INITIAL_RETRY_INTERVAL);
//
//			// Calculate the elapsed time since the start
//			long now = System.currentTimeMillis();
//			long elapsed = now - startTime;
//
//
//			// Set an appropriate interval based on the elapsed time since start 
//			if (elapsed < interval) {
//				interval = Math.min(interval * 4, MAXIMUM_RETRY_INTERVAL);
//			} else {
//				interval = INITIAL_RETRY_INTERVAL;
//			}
//			
//			Logger.log("Rescheduling connection in " + interval + "ms.");
//
//			// Save the new interval
//			mPrefs.edit().putLong(PREF_RETRY, interval).commit();
//
//			// Schedule a reconnect using the alarm manager.
//			Intent i = new Intent();
//			i.setClass(this, PushService.class);
//			i.setAction(ACTION_RECONNECT);
//			PendingIntent pi = PendingIntent.getService(this, 0, i, 0);
//			AlarmManager alarmMgr = (AlarmManager)getSystemService(ALARM_SERVICE);
//			alarmMgr.set(AlarmManager.RTC_WAKEUP, now + interval, pi);
		}
		
		// Remove the scheduled reconnect
		public void cancelReconnect() {
//			Intent i = new Intent();
//			i.setClass(this, PushService.class);
//			i.setAction(ACTION_RECONNECT);
//			PendingIntent pi = PendingIntent.getService(this, 0, i, 0);
//			AlarmManager alarmMgr = (AlarmManager)getSystemService(ALARM_SERVICE);
//			alarmMgr.cancel(pi);
		}
		
		private synchronized void reconnectIfNecessary() {		
//			if (mStarted == true && mConnection == null) {
//				Logger.log("Reconnecting...");
//				connect();
//			}
		}

		// This receiver listeners for network changes and updates the MQTT connection
		// accordingly
//		private BroadcastReceiver mConnectivityChanged = new BroadcastReceiver() {
//			@Override
//			public void onReceive(Context context, Intent intent) {
//				// Get network info
//				NetworkInfo info = (NetworkInfo)intent.getParcelableExtra (ConnectivityManager.EXTRA_NETWORK_INFO);
//				
//				// Is there connectivity?
//				boolean hasConnectivity = (info != null && info.isConnected()) ? true : false;
//
//				Logger.log("Connectivity changed: connected=" + hasConnectivity);
//
//				if (hasConnectivity) {
//					reconnectIfNecessary();
//				} else if (mConnection != null) {
//					// if there no connectivity, make sure MQTT connection is destroyed
//					mConnection.disconnect();
//					cancelReconnect();
//					mConnection = null;
//				}
//			}
//		};
		
		// Check if we are online
		private boolean isNetworkAvailable() {
			NetworkInfo info = mConnMan.getActiveNetworkInfo();
			if (info == null) {
				return false;
			}
			return info.isConnected();
		}
	}