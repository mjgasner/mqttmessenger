package com.gasner.mqttmessenger;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

public class Notifier {
	private static String NOTIF_TITLE = "MQTTMessenger";
	private static final int NOTIF_CONNECTED = 0;
	
	private static NotificationManager mNotifMan;
	
	public static void showNotification(String text, Context context) {
		mNotifMan = (NotificationManager)context.getSystemService(context.NOTIFICATION_SERVICE);
		
		Notification n = new Notification();
				
		n.flags |= Notification.FLAG_SHOW_LIGHTS;
      	n.flags |= Notification.FLAG_AUTO_CANCEL;

        n.defaults = Notification.DEFAULT_ALL;
      	
		//n.icon = com.tokudu.demo.R.drawable.icon;
		n.when = System.currentTimeMillis();

		// Simply open the parent activity
		PendingIntent pi = PendingIntent.getActivity(context, 0,
		  new Intent(context, MQTTMessengerActivity.class), 0);

		// Change the name of the notification here
		n.setLatestEventInfo(context, NOTIF_TITLE, text, pi);

		mNotifMan.notify(NOTIF_CONNECTED, n);
	}
}
