package com.gasner.sqlite;

import android.os.Parcel;
import android.os.Parcelable;

public class Contact implements Parcelable {
	private long id;
	private String gmail;
	private String name;
	private long unreadCount;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public String getGmail() {
		return gmail;
	}
	public void setGmail(String gmail) {
		this.gmail = gmail;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public long getUnreadCount() {
		return unreadCount;
	}
	public void setUnreadCount(long unreadCount) {
		this.unreadCount = unreadCount;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return name;
	}

	public Contact(){
		
	}
	
	private Contact(Parcel in){
		id = in.readLong();
		gmail = in.readString();
		name = in.readString();
		unreadCount = in.readLong();
	}
	
	public static final Parcelable.Creator<Contact> CREATOR =
			new Parcelable.Creator<Contact>() {

				public Contact createFromParcel(Parcel source) {
					return new Contact(source);
				}

				public Contact[] newArray(int size) {
					return new Contact[size];
				}
				
			};

	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(id);
		dest.writeString(gmail);
		dest.writeString(name);
		dest.writeLong(unreadCount);
	}
}
