package com.gasner.sqlite;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class ContactsDataAccess {

	public static long AddNewContact(Context context, String name) {
		MessagesDataSource ds = new MessagesDataSource(context);
		
		ContentValues values = new ContentValues();
		values.putNull(StorageManager.COLUMN_ID);
		values.put(StorageManager.COLUMN_GMAIL, name);
		values.put(StorageManager.COLUMN_NAME, name);
		values.put(StorageManager.COLUMN_UNREADCOUNT, 0);
		
		long insertid = ds.database.insert(StorageManager.TABLE_CONTACTS, null, values);
		return insertid;
	}
	
	public static ArrayList<Contact> getAllContacts(Context context) {
		ContactsDataSource ds = new ContactsDataSource(context);
		
		ArrayList<Contact> contactList = new ArrayList<Contact>();
		Cursor cursor = ds.database.query(StorageManager.TABLE_CONTACTS, ds.allColumns, 
				null, null, null, null, StorageManager.COLUMN_NAME);
		
		cursor.moveToFirst();
		while (!cursor.isAfterLast()){
			Contact contact = cursorToContact(cursor);
			contactList.add(contact);
			cursor.moveToNext();
		}
		
		cursor.close();
		ds.close();
		return contactList;
	}
	public static long getContactIdByContactGmail(Context context, String gmail) {
		ContactsDataSource ds = new ContactsDataSource(context);
		
		long contactId = 0;
		Cursor cursor = ds.database.query(
				StorageManager.TABLE_CONTACTS, 
				new String[] { StorageManager.COLUMN_GMAIL }, 
				"WHERE " + StorageManager.COLUMN_CONTACTID + " = " + gmail, 
				null, 
				null, 
				null, 
				null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			contactId = cursor.getLong(0);
			cursor.moveToNext();
		}
		
		cursor.close();
		ds.close();
		return contactId;
	}
	
	private static Contact cursorToContact(Cursor cursor) {
		Contact contact = new Contact();
		contact.setId(cursor.getLong(0));
		contact.setGmail(cursor.getString(1));
		contact.setName(cursor.getString(2));
		contact.setUnreadCount(cursor.getLong(3));
		
		return contact;
	}
}
