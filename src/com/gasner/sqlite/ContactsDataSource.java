package com.gasner.sqlite;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.widget.SimpleCursorAdapter.CursorToStringConverter;

public class ContactsDataSource {
	
	//Database fields
	public static SQLiteDatabase database;
	public static StorageManager dbManager;
	public static String[] allColumns = { 
				   StorageManager.COLUMN_ID, 
				   StorageManager.COLUMN_GMAIL, 
				   StorageManager.COLUMN_NAME,
				   StorageManager.COLUMN_UNREADCOUNT};
	
	public ContactsDataSource(Context context) {
		dbManager = new StorageManager(context);
		open();
	}
	
	public void open() throws SQLException {
		database = dbManager.getWritableDatabase();
	}
	
	public void close() {
		dbManager.close();
	}
}
