package com.gasner.sqlite;

import android.os.Parcel;
import android.os.Parcelable;

public class Message implements Parcelable {
	private long id;
	private long contactid;
	private String message;
	private String time;
	private int fromme;
	
	public long getId() {
		return id;
	}
	public void setID(long id) {
		this.id = id;
	}
	
	public long getContactId() {
		return contactid;
	}
	public void setContactId(long contactid) {
		this.contactid = contactid;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	
	public int getFromMe(){
		return fromme;
	}
	public void setFromMe(int fromme) {
		this.fromme = fromme;
	}
	public void setFromMe(long fromme){
		this.fromme = (int) fromme;
	}
	
	@Override
	public String toString() {
		return message;
	}

	public Message() {
		
	}
	
	private Message(Parcel in){
		id = in.readLong();
		contactid = in.readLong();
		message = in.readString();
		time = in.readString();
		fromme = in.readInt();
	}
	
	public static final Parcelable.Creator<Message> CREATOR = 
			new Parcelable.Creator<Message>() {
				
				public Message createFromParcel(Parcel source) {
					return new Message(source);
				}
				
				public Message[] newArray(int size){
					return new Message[size];
				}
		
			};
			
	public int describeContents() {
		return 0;
	}
	public void writeToParcel(Parcel dest, int flags){
		dest.writeLong(id);
		dest.writeLong(contactid);
		dest.writeString(message);
		dest.writeString(time);
		dest.writeInt(fromme);
	}
}
