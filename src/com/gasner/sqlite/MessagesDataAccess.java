package com.gasner.sqlite;

import java.util.ArrayList;
import java.util.List;

import com.gasner.mqttmessenger.Logger;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class MessagesDataAccess {
	public static void createMessage(Context context, long contactId, String message, String time, int fromme) {
		MessagesDataSource ds = new MessagesDataSource(context);
		
		ContentValues values = new ContentValues();
		values.putNull(StorageManager.COLUMN_ID);
		values.put(StorageManager.COLUMN_CONTACTID, contactId);
		values.put(StorageManager.COLUMN_MESSAGE, message);
		values.put(StorageManager.COLUMN_TIME, time);
		values.put(StorageManager.COLUMN_FROMME, fromme);
		
		long insertid = ds.database.insert(StorageManager.TABLE_MESSAGES, null, values);
		ds.close();
		
		AddToUnreadCount(context, contactId);
	}

	public static void AddNewMessages(Context context, List<QTMessage> newMessageList){
		Logger.log("Message Data Access - AddNewMessages");
		for(QTMessage message : newMessageList) {
			createMessage(context,
					MessagesDataAccess.getContactIdByName(context, message.getName()),
					message.getMessage(), 
					message.getTime(),
					0);
			Logger.log("Message Written");
		}
	}
	public static void AddToUnreadCount(Context context, long contactId) {
		long unreadCount = getUnreadCount(context, contactId);
		unreadCount++;
		
		MessagesDataSource ds = new MessagesDataSource(context);
		
		ContentValues values = new ContentValues();
		values.put(StorageManager.COLUMN_UNREADCOUNT, unreadCount);
		
		String where = StorageManager.COLUMN_ID + " = " + contactId;		
		ds.database.update(
				StorageManager.TABLE_CONTACTS, 
				values, 
				where,
				null);
		ds.close();
	}
	
	public static List<Message> getAllMessages(Context context) {
		MessagesDataSource ds = new MessagesDataSource(context);
		
		List<Message> messageList = new ArrayList<Message>();
		Cursor cursor = ds.database.query(StorageManager.TABLE_MESSAGES, ds.allColumns,
				null, null, null, null, StorageManager.COLUMN_TIME);
		
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			Message message = cursorToMessage(cursor);
			messageList.add(message);
			cursor.moveToNext();
		}
		
		cursor.close();
		ds.close();
		return messageList;
	}
	public static ArrayList<Message> getMessagesByContactId(Context context, long contactId) {
		MessagesDataSource ds = new MessagesDataSource(context);
		
		ArrayList<Message> messageList = new ArrayList<Message>();
		
		String sql = 
				"SELECT " + 
					StorageManager.TABLE_MESSAGES + "." + StorageManager.COLUMN_ID + ", " +
					StorageManager.TABLE_MESSAGES + "." + StorageManager.COLUMN_CONTACTID + ", " + 
					StorageManager.TABLE_MESSAGES + "." + StorageManager.COLUMN_MESSAGE + ", " +
					StorageManager.TABLE_MESSAGES + "." + StorageManager.COLUMN_TIME + ", " +
					StorageManager.TABLE_MESSAGES + "." + StorageManager.COLUMN_FROMME + " " +
				"FROM " + 
					StorageManager.TABLE_MESSAGES + " " +
				"WHERE " + 
					StorageManager.TABLE_MESSAGES + "." + StorageManager.COLUMN_CONTACTID + " = " + String.valueOf(contactId);
		
		Cursor cursor = ds.database.rawQuery(sql, null);
		
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			Message message = cursorToMessage(cursor);
			messageList.add(message);
			cursor.moveToNext();
		}
		
		cursor.close();
		ds.close();
		return messageList;
	}
	public static long getUnreadCount(Context context, long contactId) {
		MessagesDataSource ds = new MessagesDataSource(context);
		
		long unreadCount = 0;
		
		String [] columns = { StorageManager.COLUMN_UNREADCOUNT };
		Cursor cursor = ds.database.query(StorageManager.TABLE_CONTACTS, columns,
				null, null, null, null, null);
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			unreadCount = cursor.getLong(0);
			cursor.moveToNext();
		}
		
		cursor.close();
		ds.close();
		return unreadCount;
	}
	public static long getContactIdByName(Context context, String name) {
		MessagesDataSource ds = new MessagesDataSource(context);
		
		long contactid = 0;
		String [] columns = { StorageManager.COLUMN_ID };
		Cursor cursor = ds.database.query(StorageManager.TABLE_CONTACTS, columns,
				StorageManager.COLUMN_GMAIL + " = '" + name + "'", null, null, null, null);
		if(cursor.getCount() > 0) {		
			cursor.moveToFirst();
			while(!cursor.isAfterLast()) {
				contactid = cursor.getLong(0);
				cursor.moveToNext();
			}
			
			cursor.close();
			ds.close();
			return contactid;
		}
		else {
			return ContactsDataAccess.AddNewContact(context, name);
		}
	}

	private static Message cursorToMessage(Cursor cursor) {
		Message message = new Message();
		message.setID(cursor.getLong(0));
		message.setContactId(cursor.getLong(1));
		message.setMessage(cursor.getString(2));
		message.setTime(cursor.getString(3));
		message.setFromMe(cursor.getLong(4));
		
		return message;
	}

}
