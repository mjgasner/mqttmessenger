package com.gasner.sqlite;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class MessagesDataSource {
	
	public static SQLiteDatabase database;
	public static StorageManager dbManager;
	public static String [] allColumns = {
					StorageManager.COLUMN_ID,
					StorageManager.COLUMN_CONTACTID,
					StorageManager.COLUMN_MESSAGE,
					StorageManager.COLUMN_TIME };
	
	public MessagesDataSource(Context context) {
		dbManager = new StorageManager(context);
		open();
	}
	
	public void open()  throws SQLException {
		database = dbManager.getWritableDatabase();
	}
	public void close() {
		dbManager.close();
	}
	
}
