package com.gasner.sqlite;

public class QTMessage {
	private long id;
	private String contactname;
	private String message;
	private String time;
	
	public long getId() {
		return id;
	}
	public void setID(long id) {
		this.id = id;
	}
	
	public String getName() {
		return contactname;
	}
	public void setName(String name) {
		this.contactname = name;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	@Override
	public String toString() {
		return message;
	}
}
