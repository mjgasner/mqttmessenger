package com.gasner.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.*;

public class StorageManager extends SQLiteOpenHelper {

	//id column used in all tables
	public static final String COLUMN_ID = "id";
	
	//contacts table and columns
	public static final String TABLE_CONTACTS = "contacts";
	public static final String COLUMN_GMAIL = "gmail";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_UNREADCOUNT = "unreadcount";
	
	//messages table and columns
	public static final String TABLE_MESSAGES = "messages";
	public static final String COLUMN_CONTACTID = "contactid";
	public static final String COLUMN_MESSAGE = "message";
	public static final String COLUMN_TIME = "time";
	public static final String COLUMN_FROMME = "fromme";
	
	private static final String DATABASE_NAME = "mqttmessenger.db";
	private static final int DATABASE_VERSION = 1;
	
	//Database creation sql statement
	private static final String CONTACT_TABLE_CREATE = 
			//"create table contacts( id integer primary key autoincrement, gmail text not null, name text not null, unreadcount integer not null);";
			"create table " + 
			TABLE_CONTACTS + "( " + 
			COLUMN_ID + " integer primary key autoincrement, " +
			COLUMN_GMAIL + " text not null, " + 
			COLUMN_NAME + " text not null, " +
			COLUMN_UNREADCOUNT + " integer not null);";
	private static final String MESSAGE_TABLE_CREATE =
			"create table "+ 
			TABLE_MESSAGES + "( " + 
			COLUMN_ID + " integer primary key autoincrement, " +
			COLUMN_CONTACTID + " integer not null, " + 
			COLUMN_MESSAGE + " text not null, " + 
			COLUMN_TIME + " text not null, " +
			COLUMN_FROMME + " integer not null);";
	
	
	public StorageManager(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CONTACT_TABLE_CREATE);
		db.execSQL(MESSAGE_TABLE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS" + TABLE_CONTACTS);
		db.execSQL("DROP TABLE IF EXISTS" + TABLE_MESSAGES);
		
		onCreate(db);
	}
}
